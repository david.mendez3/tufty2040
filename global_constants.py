BRIGHTNESS_UPDATE_INTERVAL_MS = 500

# Skew and alternate gallery lists

SKEW_LIST = ["normal"]
ALTERNATE_GALLERY_SKEWS = []

# Asset directories

BADGE_ASSETS_DIRECTORY = "badge_assets"
QR_CODE_DIRECTORY = "qrcodes"
GALLERY_DIRECTORY = "images"
